library(httr)

accu_api_key  <- 'TqnYA8VjMpFgwjPOTLxAOeCoulGSJQGG'

accuweather <- function(city, api_key) {
  city <- URLencode(city)
  cities_url <- 'http://dataservice.accuweather.com/locations/v1/cities/search'
  cities_url <- paste0(cities_url, '?apikey=', api_key, '&q=', city, sep='')
  cities_json <- GET(url = cities_url, timeout(10))
  cities_json <- content(cities_json)
  cities_table <- lapply(cities_json, function(x) {
    data.table(
      city_key = x$Key,
      city_rank = x$Rank,
      city_localized_name = x$LocalizedName,
      city_english_name = x$EnglishName,
      city_country = x$Country$ID,
      city_area_id = x$AdministrativeArea$ID,
      city_area_name = x$AdministrativeArea$LocalizedName
    )
  })
  cities_table <- rbindlist(cities_table)
  city_key <- cities_table[city_english_name == city_area_name, city_key]
  
  
  current_weather_url <- 'http://dataservice.accuweather.com/currentconditions/v1/'
  current_weather_url <- paste(current_weather_url, city_key, '?apikey=', api_key, sep = '')
  current_weather_json <- GET(url = current_weather_url, timeout(10))
  current_weather_json <- content(current_weather_json)
  current_weather_table <- data.table(
    city = city,
    weather = current_weather_json[[1]]$WeatherText,
    precipitation = current_weather_json[[1]]$HasPrecipitation,
    temp_C = current_weather_json[[1]]$Temperature$Metric$Value,
    temp_F = current_weather_json[[1]]$Temperature$Imperial$Value
  )
  
  # forecast_weather_url <- 'http://dataservice.accuweather.com/forecasts/v1/daily/1day/'
  # forecast_weather_url <- paste(forecast_weather_url, city_key, '?apikey=', api_key, sep = '')
  # forecast_weather_json <- GET(url = forecast_weather_url, timeout(10))
  # forecast_weather_json <- content(forecast_weather_json)
  current_weather_table
}

accuweather(city = 'Москва', api_key = accu_api_key)




