---
title: "lists"
author: "Philipp A.Upravitelev <br> <upravitelev@gmail.com> <br>"
date: '`r Sys.Date()`'
output:
  ioslides_presentation:
    autosize: yes
    logo: ./pics/Rlogo.svg
    transition: slower
    widescreen: yes
  beamer_presentation: default
---

# lists


## list() {.build}

```{r}
my_ls <- list(1:5, letters[sample(5)])
```

```{r}
my_ls
```

## {.build}

```{r}
my_ls_named <- list(
  el1 = 1:5, 
  el2 = letters[sample(5)]
)
```

```{r}
my_ls_named
```

## object info {.build}

```{r}
class(my_ls_named)
```


```{r}
str(my_ls_named)
```

## выбор элементов списков {.build}

<img src="./pics/2_lists_structure.png" width="90%" style="position:absolute;">

## выбор элементов списка {.build}

```{r}
my_ls_named[1]
```

```{r}
my_ls_named$el1
```


# data.frame & data.table

## data.frame {.build}

```{r}
df <- data.frame(
  var1 = sample(letters, 5),
  var2 = sample(1:5, 5)
)
```

```{r}
print(df)
```

## {.build}
```{r}
df$var1
```

```{r}
df[1, 2]
```

```{r}
df[df$var2 <= 3, ]
```



## dplyr
```{r, message=FALSE}
library(dplyr)
dp <- as_tibble(df)
dp %>% 
  filter(var2 <= 3) 
```

# data.table

## 

```{r}
# install.packages('data.table')
library(data.table)
```


## создание data.table {.build}
```{r dt1, message=FALSE}
dt1 <- data.table(
  month_names = month.name,
  month_abb = month.abb,
  month_ord = seq_len(length(month.abb)),
  is_winter = grepl('Jan|Dec|Feb', month.abb)
)
# setDT(df)
```

```{r}
class(dt1)
```


## общая форма data.table
<div style="font-size: 62px; color: black">
DT[i, j, by]
</div>

## выбор строк

 - by position
 - by value
 
## выбор колонок по названию {.build}

```{r dt2}
dt1[, month_names]
```

```{r}
dt1[, list(month_names)]
```


## выбор нескольких колонок
```{r dt3}
dt1[, list(month_names, month_abb)]
```


## .SD-синтаксис

```{r dt4}
dt1[, .SD, .SDcols = c('month_names', 'month_abb')]
```

-------

```{r dt5}
tg_names <- names(dt1)
dt1[, .SD, .SDcols = tg_names[grep('month', tg_names)]]
```

## создание колонок
```{r dt12}
dt1[, new_col := 12:1]
dt1
```


## удаление колонок
```{r dt13}
dt1[, new_col := NULL]
```


## rbind()
```{r dt6}
# создаем первую таблицу
dt1 <- data.table(tb = 'table_1',
                  col1 = sample(9, 3),
                  col3 = 'only in table1',
                  col2 = sample(letters, 3))

# создаем вторую таблицу
dt2 <- data.table(tb = 'table_2',
                  col4 = 'only in table2',
                  col1 = sample(9, 3),
                  col2 = sample(letters, 3))
```

## {.build}
```{r dt7, eval=FALSE}
# объединяем по строкам
rbind(dt1, dt2, use.names = TRUE, fill = TRUE)
```

```{r, echo=FALSE}
rbind(dt1, dt2, use.names = TRUE, fill = TRUE)
```


## cbind() {.build}

```{r dt9, eval=FALSE}
cbind(dt1, dt2)
```

```{r, echo=FALSE}
cbind(dt1, dt2)
```


## merge()
```{r dt11}
# создаем датасет 1, в синтасисе data.table
dt1 <- data.table(key_col = c('r1', 'r2', 'r3'),
                  col_num = seq_len(3))

# создаем датасет 2, в синтасисе data.table
dt2 <- data.table(key_col = c('r3', 'r1', 'r2'),
                  col_char = c('c', 'a', 'b'))
```

----

```{r}
# сливаем построчно по значениям в колонке key_col
merge(x = dt1, y = dt2, by = 'key_col', all = FALSE)
```


-------

<img src="./pics/merge.jpg" width="80%" style="position:absolute;left:5%;">


-------

<div style="position:absolute;bottom:10%;right:6%;">
![](./pics/tzeentch2.png)
</div>
